package com.btm.experimental.alldemo;

import redis.clients.jedis.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Redis集群配置
 *
 * @Author: btm
 * @Date: 2021/1/18 9:59
 * @Description:
 */
public class RedisShardContactPool {
    private static ShardedJedisPool pool;

    static {
        // 配置Redis信息
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(100);
        config.setMaxIdle(50);
        config.setMaxWaitMillis(3000);
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);

        // 集群
        JedisShardInfo jedisShardInfo1 = new JedisShardInfo("127.0.0.1", 6379);

        // 设置Redis的密码
        jedisShardInfo1.setPassword("123456");

        List<JedisShardInfo> list = new LinkedList<JedisShardInfo>();
        list.add(jedisShardInfo1);
        pool = new ShardedJedisPool(config, list);
    }

    public static ShardedJedis getJedisContact() {
        return pool.getResource();
    }
}

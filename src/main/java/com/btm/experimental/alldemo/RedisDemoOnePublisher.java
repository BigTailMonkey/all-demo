package com.btm.experimental.alldemo;

/**
 * 模拟消息的发布与消费，一个生产者
 */
public class RedisDemoOnePublisher {

    public static void main(String[] args) throws InterruptedException {

        Thread publisherThread = new Thread(new MsgPublisher(true));
        publisherThread.setName("publisher");
        publisherThread.start();

        Thread subscriberOne = new Thread(new MsgSubscriber());
        subscriberOne.setName("subscriberOne");
        subscriberOne.start();

        Thread subscriberTwo = new Thread(new MsgSubscriber());
        subscriberTwo.setName("subscriberTwo");
        subscriberTwo.start();

        while (true) {
            Thread.sleep(10000);
        }
    }

    /**
     * 输入结果如下：
     * publisher send message[1] to [msg]:[publisher[0]:2302]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[0]:2302}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[0]:2302}
     * publisher send message[2] to [msg]:[publisher[1]:2913]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[1]:2913}
     * publisher send message[3] to [msg]:[publisher[2]:3352]
     * publisher send message[4] to [msg]:[publisher[3]:9284]
     * publisher send message[5] to [msg]:[publisher[4]:1360]
     * publisher send message[6] to [msg]:[publisher[5]:2917]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[1]:2913}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[2]:3352}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[3]:9284}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[2]:3352}
     * publisher send message[7] to [msg]:[publisher[6]:4478]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[3]:9284}
     * publisher send message[8] to [msg]:[publisher[7]:1001]
     * publisher send message[9] to [msg]:[publisher[8]:7114]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[4]:1360}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[4]:1360}
     * publisher send message[10] to [msg]:[publisher[9]:6320]
     * publisher send message[11] to [msg]:[publisher[10]:4136]
     * publisher send message[12] to [msg]:[publisher[11]:2165]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[5]:2917}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[6]:4478}
     * publisher send message[13] to [msg]:[publisher[12]:5385]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[7]:1001}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[5]:2917}
     * publisher send message[14] to [msg]:[publisher[13]:6348]
     * publisher send message[15] to [msg]:[publisher[14]:5758]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[6]:4478}
     * publisher send message[16] to [msg]:[publisher[15]:510]
     * publisher send message[17] to [msg]:[publisher[16]:6273]
     * publisher send message[18] to [msg]:[publisher[17]:4517]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[8]:7114}
     * publisher send message[19] to [msg]:[publisher[18]:85]
     * publisher send message[20] to [msg]:[publisher[19]:950]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[7]:1001}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[9]:6320}
     * publisher send message[21] to [msg]:[publisher[20]:67]
     * publisher send message[22] to [msg]:[publisher[21]:2811]
     * publisher send message[23] to [msg]:[publisher[22]:7759]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[10]:4136}
     * publisher send message[24] to [msg]:[publisher[23]:9677]
     * publisher send message[25] to [msg]:[publisher[24]:2788]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[8]:7114}
     * publisher send message[26] to [msg]:[publisher[25]:4579]
     * publisher send message[27] to [msg]:[publisher[26]:2361]
     * publisher send message[28] to [msg]:[publisher[27]:3463]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[11]:2165}
     * publisher send message[29] to [msg]:[publisher[28]:4512]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[12]:5385}
     * publisher send message[30] to [msg]:[publisher[29]:1040]
     * publisher send message[31] to [msg]:[publisher[30]:7242]
     * publisher send message[32] to [msg]:[publisher[31]:1860]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[9]:6320}
     * publisher send message[33] to [msg]:[publisher[32]:4693]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[13]:6348}
     * publisher send message[34] to [msg]:[publisher[33]:427]
     * publisher send message[35] to [msg]:[publisher[34]:2525]
     * publisher send message[36] to [msg]:[publisher[35]:5953]
     * publisher send message[37] to [msg]:[publisher[36]:438]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[10]:4136}
     * publisher send message[38] to [msg]:[publisher[37]:5343]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[14]:5758}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[11]:2165}
     * publisher send message[39] to [msg]:[publisher[38]:5465]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[15]:510}
     * publisher send message[40] to [msg]:[publisher[39]:1396]
     * publisher send message[41] to [msg]:[publisher[40]:42]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[16]:6273}
     * publisher send message[42] to [msg]:[publisher[41]:8495]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[12]:5385}
     * publisher send message[43] to [msg]:[publisher[42]:8416]
     * publisher send message[44] to [msg]:[publisher[43]:4704]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[17]:4517}
     * publisher send message[45] to [msg]:[publisher[44]:2222]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[13]:6348}
     * publisher send message[46] to [msg]:[publisher[45]:382]
     * publisher send message[47] to [msg]:[publisher[46]:8738]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[18]:85}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[14]:5758}
     * publisher send message[48] to [msg]:[publisher[47]:1745]
     * publisher send message[49] to [msg]:[publisher[48]:7448]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[15]:510}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[19]:950}
     * publisher send message[50] to [msg]:[publisher[49]:7281]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[16]:6273}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[20]:67}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[17]:4517}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[18]:85}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[19]:950}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[21]:2811}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[20]:67}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[21]:2811}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[22]:7759}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[23]:9677}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[22]:7759}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[23]:9677}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[24]:2788}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[25]:4579}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[24]:2788}
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[26]:2361}
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[25]:4579}
     * publisher send message[1] to [msg]:[publisher[0]:1130]
     * publisher send message[2] to [msg]:[publisher[1]:4564]
     * ----------------------------------------------------------------subscriberTwo listener new message {publisher[27]:3463}
     * publisher send message[3] to [msg]:[publisher[2]:6918]
     * ----------------------------------------------------------------subscriberOne listener new message {publisher[26]:2361}
     */

}

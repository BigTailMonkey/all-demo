package com.btm.experimental.alldemo;

/**
 * @Author: btm
 * @Date: 2021/1/18 10:28
 * @Description:
 */
public class RedisInfo {

    /**
     * 频道名称
     */
    public static final String SUBSCRIBE_NAME = "msg";

    /**
     * 消息分隔符
     */
    public static final String SEPARATOR = ":";

}

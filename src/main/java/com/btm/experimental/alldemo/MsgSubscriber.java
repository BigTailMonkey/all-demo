package com.btm.experimental.alldemo;

import redis.clients.jedis.Jedis;

/**
 * 消息的订阅者线程，模拟消息的消费
 *
 * @Author: btm
 * @Date: 2021/1/18 10:33
 * @Description:
 */
public class MsgSubscriber implements Runnable {

    public void run() {
        Jedis jedis = RedisContactPool.getJedisContact();
        MsgPubSubListener listener = new MsgPubSubListener();
        while (true) {
            jedis.subscribe(listener, RedisInfo.SUBSCRIBE_NAME);
        }
    }
}

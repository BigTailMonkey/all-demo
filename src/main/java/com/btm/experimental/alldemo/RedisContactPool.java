package com.btm.experimental.alldemo;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * redis单机配置
 *
 * @Author: btm
 * @Date: 2021/1/18 10:10
 * @Description:
 */
public class RedisContactPool {

    private static JedisPool jedisPool;

    static {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        jedisPool = new JedisPool(config,"127.0.0.1",6379,100000,"123456");
    }

    public static Jedis getJedisContact() {
        return jedisPool.getResource();
    }

}

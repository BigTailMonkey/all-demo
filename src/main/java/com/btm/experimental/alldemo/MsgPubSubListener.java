package com.btm.experimental.alldemo;

import redis.clients.jedis.JedisPubSub;

import java.util.Random;

/**
 * @Author: btm
 * @Date: 2021/1/18 10:05
 * @Description:
 */
public class MsgPubSubListener extends JedisPubSub {

    @Override
    public void onMessage(String channel, String message) {
//        System.out.println("channel:" + channel + " receives message :" + message);
        Random random = new Random();
        Integer sleep = random.nextInt(3000);
        try {
            Thread.sleep(sleep);
            System.out.println("----------------------------------------------------------------" + Thread.currentThread().getName() + " listener new message {" + message + "}");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
//        System.out.println("channel:" + channel + " is been subscribed:" + subscribedChannels);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
//        System.out.println("channel:" + channel + " is been unsubscribed:" + subscribedChannels);
    }
}

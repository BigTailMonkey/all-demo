# all-demo

#### 介绍
特性、功能测试与实验用例。按分支区分，一个分支即为一个特性或功能。

#### 分支介绍
订阅/发布的顺序性，结论：单个订阅者的消息消费是有序的。
若前一个消息处理的过慢，则会阻塞后续消息的处理。